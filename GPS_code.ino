#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Servo.h>
Servo myservo;
int pos = 0;

static const uint32_t GPSBaud = 9600;
static const int STEPPER_TURN_STEPS = 100;

//Primary stepper motor controller
static const int STEPPER_DIRECTION = 4;
static const int STEPPER_STEP = 5;
static const int STEPPER_ENABLE_LOW = 6;

//Backup stepper motor controller -- comment out unless primary is not working
//static const int STEPPER_DIRECTION = 9;
//static const int STEPPER_STEP = 10;
//static const int STEPPER_ENABLE_LOW = 11;

static const int DRIVE_MOTOR_RELAY = 13;

// GPS coordinates of places to go
static const double LAT_2KR = 43.049300;
static const double LNG_2KR = -70.691242;

static const double LAT_SHOALS_RED_MARK = 42.993071;
static const double LNG_SHOALS_RED_MARK = -70.620418;

static const double LAT_SAGAMORE_RED_20 = 43.054331;
static const double LNG_SAGAMORE_RED_20 = -70.742754;

static const double LAT_SAGAMORE_RED_18 = 43.055111;
static const double LNG_SAGAMORE_RED_18 = -70.741714;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(11, 10);

int count;

double f_lat;
double f_lng;
double course_to_target;
double t_lat; //target latitude
double t_lng; //target lnggitude

boolean reportAllAhead = false;

double courseToTarget(double lat, double lng) {
  return gps.courseTo(lat, lng, t_lat, t_lng);
}

double rangeToTarget(double lat, double lng) {
  return gps.distanceBetween(lat, lng, t_lat, t_lng);
}

void setTarget(double lat, double lng) {
  t_lat = lat;
  t_lng = lng;
}

void readGps() {
  while (!(gps.location.isValid() && gps.location.isUpdated())) {
    while (ss.available() > 0) gps.encode(ss.read());
  }
}

void reportLocation(double lat, double lng) {
  Serial.print(lat, 9);
  Serial.print(" ");
  Serial.print(lng, 9);
  Serial.print(" ");
  Serial.print("Distance to target: ");
  Serial.print(rangeToTarget(lat, lng), 2);
  Serial.print(" Course to target: ");
  Serial.println(courseToTarget(lat, lng), 2);
}

void storeFix() {
  f_lat = gps.location.lat();
  f_lng = gps.location.lng();
  course_to_target = courseToTarget(f_lat, f_lng);
}

void allAheadFull() {
  if (!reportAllAhead) {
    Serial.println("ALL AHEAD FULL");
    reportAllAhead = true;
  }
  
  digitalWrite(DRIVE_MOTOR_RELAY, HIGH);
}

void allStop() {
  Serial.println("ALL STOP");
  digitalWrite(DRIVE_MOTOR_RELAY, LOW);
  digitalWrite(STEPPER_ENABLE_LOW, HIGH);
}

void setup() {
  Serial.begin(115200);
  ss.begin(GPSBaud);
  // Set destination.
  setTarget(LAT_SHOALS_RED_MARK, LNG_SHOALS_RED_MARK);

  Serial.println("Starting GPS...");
  readGps();
  storeFix();
  reportLocation(f_lat, f_lng);
  Serial.println("Setup complete");
  allAheadFull();
}

void loop() { 
    readGps();
    double new_lat = gps.location.lat();
    double new_lng = gps.location.lng();
    allAheadFull();
    double distance = gps.distanceBetween(new_lat, new_lng, f_lat, f_lng);
    double altitude = gps.altitude.meters();
    Serial.print("CURRENT ALTITUDE = ");
    Serial.print(gps.altitude.meters());
    Serial.println("");
    
}
