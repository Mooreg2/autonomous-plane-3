#include <Servo.h> 

Servo esc;

int pwm = 8;
int min = 1000;
int max = 2000;

void setup() {
  esc.attach(pwm);
  
  //arm esc -- mimics stick movements from radio control
  //go to min, wait, then max, wait, then min
  esc.writeMicroseconds(min);
  delay(2000);
  esc.writeMicroseconds(max);
  delay(2000);
  esc.writeMicroseconds(min);
  delay(2000);
  
  // test motor
  esc.writeMicroseconds(1200);
  delay(2000);
  esc.writeMicroseconds(1500);
  delay(2000);
  esc.writeMicroseconds(1900);
  delay(2000);
  esc.writeMicroseconds(1000);
}

void loop() {
  // put your main code here, to run repeatedly:

}
